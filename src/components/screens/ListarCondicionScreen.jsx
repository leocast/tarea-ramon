import React, { useEffect, useState } from 'react'
import { useFetch } from '../../global/hooks/useFetch'

const TrTable = ({id}) => (
    <tr>
        <td>{id}</td>
    </tr>
)

export const ListarCondicionScreen = () => {

    const [inputValue, setInputValue] = useState('')
    const [idJuego, setIdJuego] = useState('')

    const [{data, isLoading}] = useFetch(`http://187.189.158.166:17080/MAA9/Api/consulta2-juego.php?id=${idJuego}`)

    const [idsJuegos, setIdsJuegos] = useState([])
    const [validMemoLeak, setMemoLeak] = useState('')


    useEffect(()=> {
        fetch(`http://187.189.158.166:17080/MAA9/Api/consulta-juego.php`)
        .then(res => res.json())
        .then(json => {
            setIdsJuegos(json.Articulos.map(juego => juego.idJuego))
        })
        .catch(e => {
            console.log(e);
        });
    }, [validMemoLeak])

    const handleInputChange = ({target}) => {
        
        if(target.value.length > 8){
            target.value = inputValue
            return false
        }

        setInputValue(target.value)
    }

    const handleButtonClick = () => {
        if(inputValue.trim() === '')
            return alert('El id está vacío')

        setIdJuego(inputValue)
    }

    return (
        <div className="limpiarRegistros">
            <h1>Buscar por id</h1>
            <hr />
            <div className="row">
                <div className="col-6">
                    <div className="limpiarRegistros__form">
                        <label htmlFor="">Id del juego a buscar:</label>
                        <input 
                            type="number" 
                            className="form-control"
                            onChange={handleInputChange} />
                        <button 
                            type="button" 
                            className="mt-3 btn btn-dark"
                            onClick={handleButtonClick}
                            >
                            Buscar
                        </button>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col-12">
                            {
                                !isLoading 
                                ?
                                data?.ok === 0 && data?.msg.includes('Error')
                                ? 
                                ''
                                :
                                data?.ok === 0 && data?.msg.includes('No hay')
                                ?
                                'No hay games'
                                :
                                <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Descripción</th>
                                        <th>Precio</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{data.Juegos[0].idJuego}</td>
                                        <td>{data.Juegos[0].nombre}</td>
                                        <td>{data.Juegos[0].descripcion}</td>
                                        <td>{data.Juegos[0].precio}</td>
                                    </tr>
                                </tbody>
                                </table>
                                :
                                ''
                            }
                        </div>
                    </div>
                </div>
                <div className="col-6">
                    <table className="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id del juego</th>
                            </tr>
                        </thead>
                        <tbody>
                            {idsJuegos.map(juego => <TrTable key={juego} id={juego}/>)}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        )
}
