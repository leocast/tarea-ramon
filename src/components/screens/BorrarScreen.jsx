import React, { useState, useEffect } from 'react'
import { useFetch } from '../../global/hooks/useFetch'

const TrTable = ({id}) => (
    <tr>
        <td>{id}</td>
    </tr>
)

export const BorrarScreen = () => {

    const [inputValue, setInputValue] = useState('')
    const [idsJuegos, setIdsJuegos] = useState([])

    const [validMemoLeak, setMemoLeak] = useState('')

    useEffect(()=> {
        fetch(`http://187.189.158.166:17080/MAA9/Api/consulta-juego.php`)
        .then(res => res.json())
        .then(json => {
            setIdsJuegos(json.Articulos.map(juego => juego.idJuego))
        })
        .catch(e => {
            console.log(e);
        });
    }, [validMemoLeak])

    const handleInputChange = ({target}) => {
        
        if(target.value.length > 8){
            target.value = inputValue
            return false
        }

        setInputValue(target.value)
    }

    const handleButtonClick = () => {
        if(inputValue.trim() === '')
            return alert('El id está vacío')

        const esta = idsJuegos.some(idJuego => idJuego === inputValue)

        if(!esta)
            return alert('El id no existe')

        const temp = idsJuegos.filter(idJuego => idJuego !== inputValue)

        fetch(`http://187.189.158.166:17080/MAA9/Api/eliminar-juego.php?id=${inputValue}`)
            .then(res => res.json())
            .then(json => {
                console.log(json)
                setIdsJuegos(temp)
                setInputValue('')

                setMemoLeak(validMemoLeak + '.')
            })
            .catch(e => {
                console.log(e);
            });
    }

    return (
        <div className="limpiarRegistros">
            <h1>Borrar</h1>
            <hr />
            <div className="row">
                <div className="col-6">
                    <div className="limpiarRegistros__form">
                        <label htmlFor="">Id del juego a borrar:</label>
                        <input 
                            type="text" 
                            className="form-control"
                            value={inputValue}
                            onChange={handleInputChange} />
                        <button 
                            type="button" 
                            className="mt-3 btn btn-danger"
                            onClick={handleButtonClick}
                            >
                            Eliminar
                        </button>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col-12">
                       
                        </div>
                    </div>
                </div>
                <div className="col-6">
                    <table className="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id del juego</th>
                            </tr>
                        </thead>
                        <tbody>
                            {idsJuegos.map(juego => <TrTable key={juego} id={juego}/>)}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        )
}
