import React from 'react'
import '../../styles/main.sass'

export const AppScreen = () => {
    return (
        <div className="app">
            <div className="app__title">
            <div className="card mt-5">
                <h5 className="card-header"></h5>
                <div className="card-body">
                    <div>
                        <img src="https://cdn.pixabay.com/photo/2018/09/05/06/19/signal-3655575_960_720.png" alt="" />
                    </div>
                    <div>
                        <h3 className="card-title">¿Qué somos?</h3>
                        <p className="card-text">Somos una empresa que vende videojuegos bien chidoris</p>
                    </div>
                </div>
                </div>
            </div>
        </div>
    )
}
