import React from 'react'
import { useFetch } from '../../global/hooks/useFetch'


const TrTable = ({idJuego, nombre, descripcion, precio}) => (
    <tr>
        <td>{idJuego}</td>
        <td>{nombre}</td>
        <td>{descripcion}</td>
        <td>{precio}</td>
    </tr>
)

export const ListarRegistrosScreen = () => {

    const [{data, isLoading}] = useFetch('http://187.189.158.166:17080/MAA9/Api/consulta-juego.php')

    return (
        <div className="limpiarRegistros">
            <h1>Lista de juegos</h1>
            <hr />
            <div> {isLoading ? 'Cargando...': ''} </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data !== undefined && data !== null
                        ?
                            data.Articulos.map(articulo => <TrTable key={articulo.idJuego} {...articulo}/> )
                        :
                        ''
                    }   
                </tbody>
            </table>
        </div>
    )
}
