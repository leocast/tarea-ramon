import React from 'react'
import Swal from 'sweetalert2'
import { useForm } from '../../global/hooks/useForm'

export const AgregarScreen = ({isEdit = false}) => {

    const formConfig = [
        {
            name: 'id',
            value: '',
            emptyMessage: 'El id no puede estar vacío'
        },
        {
            name: 'nombre',
            value: '',
            emptyMessage: 'El nombre no puede estar vacío',
            treatmentMessage: 'El nombre debe tener más de 4 caracteres como mínimo'
        },
        {
            name: 'descripcion',
            value: '',
            emptyMessage: 'La descripción no puede estar vacía',
            treatmentMessage: 'La descripción debe tener más de 4 caracteres como mínimo'
        },
        {
            name: 'precio',
            value: '',
            emptyMessage: 'El precio no puede estar vacío',
            treatmentMessage: 'El precio debe estar entre 0 y 10,000'
        },
    ]

    const [form, handleInputChange, drawEmptyError, validate, cleanForm] = useForm(formConfig)

    const [id, nombre, descripcion, precio] = form

    const treatmentName = () => nombre.value.length > 3
    const treatmentDesc = () => descripcion.value.length > 3
    const treatmentPrecio = () => precio.value > 0 && precio.value <= 10000 
    
    const agregar = () => {

        const juego = {
            id: parseInt(id.value),
            nombre: nombre.value,
            descripcion: descripcion.value,
            precio: parseFloat(precio.value)
        }

        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(juego)
        }

        const url = isEdit
            ? 'http://187.189.158.166:17080/MAA9/Api/modificar-juego.php'
            : 'http://187.189.158.166:17080/MAA9/Api/crear-juego.php'

        fetch(url, fetchConfig)
            .then(r => r.json())
            .then(result => {
                const saConfig = {
                    title: isEdit && !result.ok
                        ? 'El juego que intentó modificar no existe'
                        : isEdit
                        ? 'Se ha modificado el juego correctamente'
                        : 'Se ha insertado el juego correctamente!',
                    timer: 2000,
                    timerProgressBar: true,
                    showConfirmButton: false,
                    icon: 
                        isEdit && result.ok === 0
                        ? 'error'
                        : 'success'
                }

                Swal.fire(saConfig)

                if(!(isEdit && !result.ok))
                    cleanForm()
            })
            .catch(error => {
                const saConfig = {
                    title: 'Ha ocurrido un error al registrar!',
                    timer: 2000,
                    timerProgressBar: true,
                    showConfirmButton: false,
                    icon: 'error'
                }
                Swal.fire(saConfig)
                console.log(error)
            })
    }

    return (
        <div className="limpiarRegistros">
            <h1>{isEdit ? 'Modificar' : 'Agregar'}</h1>
            <hr />
            <div className="row limpiarRegistros__container">
                <div className="col-6">
                    <div className="limpiarRegistros__form">
                        <label htmlFor="">Id del juego:</label>
                        <input 
                            type="number" 
                            className="form-control"
                            name="id"
                            autoComplete="off"
                            value={id.value}
                            onChange={handleInputChange} 
                            />
                        <label className="limpiarRegistros__error-label text-danger">
                            {drawEmptyError(id)}
                        </label>
                        <label htmlFor="">Nombre:</label>
                        <input 
                            autoComplete="off"
                            type="text" 
                            name="nombre"
                            value={nombre.value}
                            className="form-control"
                            onChange={handleInputChange} 
                            />
                        <label className="limpiarRegistros__error-label text-danger">
                        {drawEmptyError(nombre, treatmentName)}
                        </label>
                        <label htmlFor="">Descripcion:</label>
                        <input 
                            autoComplete="off"
                            type="text" 
                            className="form-control"
                            name="descripcion"
                            value={descripcion.value}
                            onChange={handleInputChange} 
                            />
                        <label className="limpiarRegistros__error-label text-danger">
                            {drawEmptyError(descripcion, treatmentDesc)}
                        </label>
                        <label htmlFor="">Precio:</label>
                        <input 
                            autoComplete="off"
                            type="number" 
                            className="form-control"
                            name="precio"
                            value={precio.value}
                            onChange={handleInputChange} 
                            />
                        <label className="limpiarRegistros__error-label text-danger">
                            {drawEmptyError(precio, treatmentPrecio)}
                        </label>
                            <div>
                                <button 
                                    type="button" 
                                    className="mt-3 btn btn-dark float-end"
                                    onClick={agregar}
                                    disabled={'disabled' && !validate([treatmentName, treatmentDesc, treatmentPrecio])}
                                    >
                                    Agregar
                                </button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
