import React from 'react'
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'
import { AgregarScreen } from '../components/screens/AgregarScreen'
import { AppScreen } from '../components/screens/AppScreen'
import { BorrarScreen } from '../components/screens/BorrarScreen'
import { ListarCondicionScreen } from '../components/screens/ListarCondicionScreen'
import { ListarRegistrosScreen } from '../components/screens/ListarRegistrosScreen'
import { Navbar } from '../global/components/Navbar'

export const AppRouter = () => {
    return (
        <Router>
            <Navbar/>
            <div className="container">
                <Switch>
                    <Route exact path="/" component={AppScreen}/>
                    <Route exact path="/listarRegistros" component={ListarRegistrosScreen}/>
                    <Route exact path="/listarCondicion" component={ListarCondicionScreen}/>
                    <Route exact path="/borrar" component={BorrarScreen}/>
                    <Route exact path="/agregar" component={AgregarScreen}/>
                    <Route exact path="/modificar" render={(props)=> <AgregarScreen isEdit {...props}/> }/>
                    <Redirect to="/"/>
                </Switch>
            </div>
            <footer>
                Jesus Rámon Matus Alvarez - 30/06/2021 - Desarrollo de Aplicaciones Web - Julio César Flores López - 6to Semestre 
            </footer>
        </Router>
    )
}
