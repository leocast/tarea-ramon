import React, { useState, useEffect,useRef } from 'react'

export const useFetch = url => {

    const isMounted = useRef(true)

    const [state, setState] = useState({
        data: null,
        isLoading: true,
        error: null,
    })

    useEffect(()=> {
        return () => {
            isMounted.current = false
        }
    }, [])

    useEffect(() => {

        setState({...state, loading: true})  

        fetch(url).then(res => res.json()).then(data => {
                if(isMounted.current) 
                    setState(
                    {
                        data: data,
                        isLoading: false,
                        error: null,
                    })  
                else
                    console.log("Se previno el setState")
        })

    }, [url])

    return [state]
}