import { useState } from 'react'

export const useForm = (inititalState = {}, callback = () => {}) => {
    
    const [form, setForm ] = useState(inititalState)

    const handleInputChange = ({target}) => {
        
        const indexOfInput = form.findIndex(input => input.name === target.name)
        
        const tempForm = [...form]

        tempForm[indexOfInput].value = target.value

        setForm(tempForm)

        callback(tempForm)
    } 

    const drawEmptyError = (input, treatment = null) => {

        if(input.value === '')
            return input.emptyMessage

        if(treatment !== null)
             return !treatment() ? input.treatmentMessage : ''
       
        return ''
    }


    const validate = treatments => {
        const areEmptys = form.some(input => input.value === '') 

        const validTreatments = treatments
                                    .map(treatment => treatment())
                                    .every(results => results === true)

        return !areEmptys && validTreatments
    }

    const cleanForm = () => {
        
        let temp = form

        temp = temp.map(input => ({...input, value : ''}))

        setForm(temp)
    }
    
    return [form, handleInputChange, drawEmptyError, validate, cleanForm]
}