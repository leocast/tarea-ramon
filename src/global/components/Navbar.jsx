import React from 'react'
import {Link, NavLink} from "react-router-dom";

export const Navbar = () => 
(
<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
    <div className="container-fluid">
        <NavLink exact className="navbar-brand" to='/'>Game Store</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
            <NavLink exact activeClassName="active" className="nav-link" to='./'>Home</NavLink>
            <NavLink exact activeClassName="active" className="nav-link" to='./listarRegistros'>Listar registros</NavLink>
            <NavLink exact activeClassName="active" className="nav-link" to='./listarCondicion'>Listar con condición</NavLink>
            <NavLink exact activeClassName="active" className="nav-link" to='./borrar'>Borrar</NavLink>
            <NavLink exact activeClassName="active" className="nav-link" to='./modificar'>Modificar</NavLink>
            <NavLink exact activeClassName="active" className="nav-link" to='./agregar'>Agregar</NavLink>
        </div>
        </div>
    </div>
</nav>
)